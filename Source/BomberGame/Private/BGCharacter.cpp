// Bomber Game. All Rights Reserved.

#include "BGCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "BGBomb.h"
#include "BGGameModeBase.h"

ABGCharacter::ABGCharacter()
{
    PrimaryActorTick.bCanEverTick = true;

    GetCapsuleComponent()->SetCollisionProfileName("Pawn");
    GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
    GetCapsuleComponent()->SetNotifyRigidBodyCollision(true);
    GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
    bUseControllerRotationYaw = false;
    if (GetCharacterMovement())
    {
        GetCharacterMovement()->bOrientRotationToMovement = true;
        GetCharacterMovement()->RotationRate.Yaw = 720.0f;
    }
}

void ABGCharacter::BeginPlay()
{
    Super::BeginPlay();

    DefaultSpeed = GetCharacterMovement()->MaxWalkSpeed;
    OnTakeAnyDamage.AddDynamic(this, &ABGCharacter::OnTakeDamage);
}

void ABGCharacter::OnDeath()
{
    GetMesh()->SetSimulatePhysics(true);
    GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    GetCharacterMovement()->DisableMovement();
    GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    SetLifeSpan(LifeSpanOnDeath);
    bCanSetBomb = false;
    bIsDead = true;
}

void ABGCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void ABGCharacter::SetBomb()
{
    if (!bCanSetBomb) return;

    const FTransform SpawnTransform(GetGridLocation(GetActorLocation(), GridSize));

    auto BombActor = GetWorld()->SpawnActorDeferred<ABGBomb>(BombActorClass, SpawnTransform);
    if (BombActor)
    {
        BombActor->SetOwner(this);
        BombActor->SetInstigator(this);
        BombActor->SetBlastLength(GridSize * CurrentBlastLengthModifier);
        BombActor->FinishSpawning(SpawnTransform);

        BombActor->OnDestroyed.AddDynamic(this, &ABGCharacter::DecreaseSpawnedBombCount);
        IncreaseSpawnedBombCount();

        UE_LOG(LogTemp, Display, TEXT("Set Bomb"));
    }
}

void ABGCharacter::CheckSpawnedBombCount()
{
    if (bIsDead) return;

    if (CurrentBombCount == MaxBombCount)
    {
        bCanSetBomb = false;
    }
    else
    {
        bCanSetBomb = true;
    }
    UE_LOG(LogTemp, Display, TEXT("Spawned bomb amount: %i"), CurrentBombCount);
}

void ABGCharacter::IncreaseSpawnedBombCount()
{
    CurrentBombCount++;
    CheckSpawnedBombCount();
}

void ABGCharacter::DecreaseSpawnedBombCount(AActor* DestroyedActor)
{
    if (DestroyedActor->IsOwnedBy(this))
    {
        UE_LOG(LogTemp, Display, TEXT("DecreaseSpawnedBombCount IsOwnedBy(%s)"), *GetName());
        CurrentBombCount--;
        CheckSpawnedBombCount();
    }
}

FVector ABGCharacter::GetGridLocation(const FVector& NonGridLocation, float SizeOfGrid)
{
    return FVector(FMath::GridSnap(NonGridLocation.X, SizeOfGrid), FMath::GridSnap(NonGridLocation.Y, SizeOfGrid), HightOfBombSpawn);
}

void ABGCharacter::Killed(AController* KilledController)
{
    if (!GetWorld()) return;

    const auto GameMode = Cast<ABGGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!GameMode) return;

    GameMode->Killed(KilledController, GetController());
}

void ABGCharacter::OnTakeDamage(
    AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
    UE_LOG(LogTemp, Warning, TEXT("I, %s, took damage!"), *GetName());

    Killed(InstigatedBy);
    OnDeath();
}

bool ABGCharacter::TryToAddBomb(int32 BombAmount)
{
    MaxBombCount = MaxBombCount + BombAmount;
    UE_LOG(LogTemp, Warning, TEXT("MaxBombCount %i "), MaxBombCount);
    return true;
}

bool ABGCharacter::TryToAddBlastLengthModifier(int32 AddModifier)
{
    CurrentBlastLengthModifier = CurrentBlastLengthModifier + AddModifier;
    UE_LOG(LogTemp, Warning, TEXT("CurrentBlastLengthModifier %i "), CurrentBlastLengthModifier);
    return true;
}

bool ABGCharacter::TryToSetNewSpeed(float NewSpeedModifier, bool IsTemporary, float Time)
{
    GetCharacterMovement()->MaxWalkSpeed = GetCharacterMovement()->MaxWalkSpeed * NewSpeedModifier;
    if (IsTemporary)
    {
        GetWorldTimerManager().SetTimer(SpeedTimerHandle, this, &ABGCharacter::SetDefaultSpeed, Time);
    }
    UE_LOG(LogTemp, Warning, TEXT("Current Speed %f "), GetCharacterMovement()->MaxWalkSpeed);
    return true;
}

void ABGCharacter::SetDefaultSpeed()
{
    GetCharacterMovement()->MaxWalkSpeed = DefaultSpeed;
    GetWorldTimerManager().ClearTimer(SpeedTimerHandle);
    UE_LOG(LogTemp, Warning, TEXT("Current Speed %f "), GetCharacterMovement()->MaxWalkSpeed);
}

void ABGCharacter::SetPlayerColor(const FLinearColor& Color)
{
    UE_LOG(LogTemp, Warning, TEXT("SetPlayerColor %f "));
    const auto MaterialInst = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
    if (!MaterialInst) return;

    MaterialInst->SetVectorParameterValue(MaterialColorName, Color);
}
