// Bomber Game. All Rights Reserved.

#include "Pickups/PickupSpeedBoost.h"
#include "BGCharacter.h"

bool APickupSpeedBoost::GivePickupTo(APawn* PlayerPawn)
{
    const auto Character = Cast<ABGCharacter>(PlayerPawn);
    if (Character)
    {
        return Character->TryToSetNewSpeed(SpeedModifier, bIsTemporary, Time);
        UE_LOG(LogTemp, Display, TEXT("SpeedModifier was taken"));
    }
    return false;
}
