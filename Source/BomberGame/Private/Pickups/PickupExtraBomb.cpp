// Bomber Game. All Rights Reserved.

#include "Pickups/PickupExtraBomb.h"
#include "BGCharacter.h"

bool APickupExtraBomb::GivePickupTo(APawn* PlayerPawn)
{
    const auto Character = Cast<ABGCharacter>(PlayerPawn);
    if (Character)
    {
        return Character->TryToAddBomb(BombAmount);
        UE_LOG(LogTemp, Display, TEXT("Bomb was added"));
    }

    return false;
}
