// Bomber Game. All Rights Reserved.


#include "Pickups/PickupExplosionRadius.h"
#include "BGCharacter.h"

bool APickupExplosionRadius::GivePickupTo(APawn* PlayerPawn)
{
        const auto Character = Cast<ABGCharacter>(PlayerPawn);
    if (Character)
    {
        return Character->TryToAddBlastLengthModifier(BlastLengthModifier);
        UE_LOG(LogTemp, Display, TEXT("BlastLengthModifier was added"));
    }
    return false;
}
