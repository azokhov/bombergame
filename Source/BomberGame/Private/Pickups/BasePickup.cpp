// Bomber Game. All Rights Reserved.

#include "Pickups/BasePickup.h"
#include "Components/SphereComponent.h"

ABasePickup::ABasePickup()
{
    PrimaryActorTick.bCanEverTick = true;

    CollisionComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
    CollisionComponent->InitSphereRadius(50.0f);
    CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
    CollisionComponent->SetCanEverAffectNavigation(false);
    SetRootComponent(CollisionComponent);
}

void ABasePickup::BeginPlay()
{
    Super::BeginPlay();
}

void ABasePickup::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void ABasePickup::NotifyActorBeginOverlap(AActor* OtherActor)
{
    Super::NotifyActorBeginOverlap(OtherActor);

    const auto Pawn = Cast<APawn>(OtherActor);
    if (GivePickupTo(Pawn))
    {
        PickupWasTaken(OtherActor);
    }
}

bool ABasePickup::GivePickupTo(APawn* PlayerPawn)
{
    return false;
}

void ABasePickup::PickupWasTaken(AActor* OtherActor)
{
    UE_LOG(LogTemp, Display, TEXT("PickupWasTaken by %s"), *OtherActor->GetName());
    Destroy();
}
