// Bomber Game. All Rights Reserved.

#include "BGPlayerCharacter.h"

void ABGPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis("MoveForward", this, &ABGPlayerCharacter::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &ABGPlayerCharacter::MoveRight);
    PlayerInputComponent->BindAction("SetBomb", IE_Released, this, &ABGCharacter::SetBomb);
}

void ABGPlayerCharacter::MoveForward(float Amount)
{
    if (FMath::IsNearlyZero(Amount)) return;
    AddMovementInput(FVector::ForwardVector, Amount);
}

void ABGPlayerCharacter::MoveRight(float Amount)
{
    if (FMath::IsNearlyZero(Amount)) return;
    AddMovementInput(FVector::RightVector, Amount);
}
