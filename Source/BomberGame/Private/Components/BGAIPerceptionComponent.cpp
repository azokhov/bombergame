// Bomber Game. All Rights Reserved.

#include "Components/BGAIPerceptionComponent.h"
#include "AIController.h"
#include "BGCharacter.h"
#include "Perception/AISense_Sight.h"

AActor* UBGAIPerceptionComponent::GetClosestEnemy() const
{
    TArray<AActor*> PercieveActors;
    GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PercieveActors);
    if (PercieveActors.Num() == 0) return nullptr;

    const auto Controller = Cast<AAIController>(GetOwner());
    if (!Controller) return nullptr;

    const auto Pawn = Controller->GetPawn();
    if (!Pawn) return nullptr;

    float BestDistance = MAX_FLT;
    AActor* BestPawn = nullptr;

    for (const auto PercieveActor : PercieveActors)
    {
        const auto PercieveBomber = Cast<ABGCharacter>(PercieveActor);
        if (PercieveBomber && PercieveBomber->IsAlive())
        {
            const auto CurrentDistance = (PercieveActor->GetActorLocation() - Pawn->GetActorLocation()).Size();
            if (CurrentDistance < BestDistance)
            {
                BestDistance = CurrentDistance;
                BestPawn = PercieveActor;
            }
        }
        else
        {
            UE_LOG(LogTemp, Warning, TEXT("PercieveBomber invalid!!!"));
        }
    }

    return BestPawn;
}
