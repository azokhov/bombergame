// Bomber Game. All Rights Reserved.

#include "BGPlayerState.h"

DEFINE_LOG_CATEGORY_STATIC(LogBGPlayerState, All, All)

void ABGPlayerState::LogInfo()
{
    UE_LOG(LogBGPlayerState, Display, TEXT("Player:%i, Kills: %i, Deaths: %i"), TeamID, KillsNum, DeathsNum);
}
