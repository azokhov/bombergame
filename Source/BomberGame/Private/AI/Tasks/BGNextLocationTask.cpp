// Bomber Game. All Rights Reserved.

#include "AI/Tasks/BGNextLocationTask.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "NavigationSystem.h"

UBGNextLocationTask::UBGNextLocationTask()
{
    NodeName = "Next Location";
}

EBTNodeResult::Type UBGNextLocationTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    const auto Controller = OwnerComp.GetAIOwner();
    const auto BlackBoard = OwnerComp.GetBlackboardComponent();
    if (!Controller || !BlackBoard) return EBTNodeResult::Failed;

    const auto Pawn = Controller->GetPawn();
    if (!Pawn) return EBTNodeResult::Failed;

    const auto NavSys = UNavigationSystemV1::GetCurrent(Pawn);
    if (!NavSys) return EBTNodeResult::Failed;

    FNavLocation NavLocation;

    auto Location = Pawn->GetActorLocation();
    if (!SelfCenter)
    {
        auto CenterActor = Cast<AActor>(BlackBoard->GetValueAsObject(CenterActorKey.SelectedKeyName));
        if (!CenterActor) return EBTNodeResult::Failed;

        Location = CenterActor->GetActorLocation();
    }

    const auto Found = NavSys->GetRandomReachablePointInRadius(Location, Radius, NavLocation);
    if (!Found) return EBTNodeResult::Failed;

    FVector RandomPoint = GetGridLocation(NavLocation.Location, 100.0f);
    if (FMath::RandBool())
    {
        RandomPoint.X = 0.0f;
    }
    else
    {
        RandomPoint.Y = 0.0f;
    }

    BlackBoard->SetValueAsVector(AimLocationKey.SelectedKeyName, RandomPoint);
    return EBTNodeResult::Succeeded;
}

FVector UBGNextLocationTask::GetGridLocation(const FVector& NonGridLocation, float SizeOfGrid)
{
    FVector SnappedPosition;

    SnappedPosition.X = FMath::RoundToInt(NonGridLocation.X / SizeOfGrid) * SizeOfGrid;
    SnappedPosition.Y = FMath::RoundToInt(NonGridLocation.Y / SizeOfGrid) * SizeOfGrid;

    return SnappedPosition;
}
