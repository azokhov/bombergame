// Bomber Game. All Rights Reserved.

#include "AI/Services/BGSetBombService.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "BGCharacter.h"

UBGSetBombService::UBGSetBombService()
{
    NodeName = "SetBomb";
}

void UBGSetBombService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

    const auto Controller = OwnerComp.GetAIOwner();
    const auto BlackBoard = OwnerComp.GetBlackboardComponent();

    const auto HasAim = BlackBoard && BlackBoard->GetValueAsObject(EnemyActorKey.SelectedKeyName);

    if (Controller)
    {
        const auto Bomber = Cast<ABGCharacter>(Controller->GetPawn());
        if (Bomber && HasAim)
        {
            Bomber->SetBomb();
        }
    }
}
