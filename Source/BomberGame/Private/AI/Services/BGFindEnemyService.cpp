// Bomber Game. All Rights Reserved.

#include "AI/Services/BGFindEnemyService.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "Components/BGAIPerceptionComponent.h"

UBGFindEnemyService::UBGFindEnemyService()
{
    NodeName = "Find Enemy";
}

void UBGFindEnemyService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

    UE_LOG(LogTemp, Display, TEXT("Find enemy"));

    const auto BlackBoard = OwnerComp.GetBlackboardComponent();
    if (BlackBoard)
    {
        const auto Controller = OwnerComp.GetAIOwner();
        const auto PerceptionComponent = Controller->FindComponentByClass<UBGAIPerceptionComponent>();
        if (PerceptionComponent)
        {
            BlackBoard->SetValueAsObject(EnemyActorKey.SelectedKeyName, PerceptionComponent->GetClosestEnemy());
        }
        else
        {
            UE_LOG(LogTemp, Warning, TEXT("PerceptionComponent invalid"));
        }
    }
}
