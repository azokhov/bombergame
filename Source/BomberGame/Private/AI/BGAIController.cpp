// Bomber Game. All Rights Reserved.

#include "AI/BGAIController.h"
#include "AI/BGAICharacter.h"
#include "Components/BGAIPerceptionComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

ABGAIController::ABGAIController()
{
    BGAIPerceptionComponent = CreateDefaultSubobject<UBGAIPerceptionComponent>("BGPerceptionComponent");
    SetPerceptionComponent(*BGAIPerceptionComponent);

    bWantsPlayerState = true;
}

void ABGAIController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    const auto BGCharacter = Cast<ABGAICharacter>(InPawn);
    if (BGCharacter)
    {
        RunBehaviorTree(BGCharacter->BehaviorTreeAsset);
    }
}

void ABGAIController::Tick(float DelataTime)
{
    Super::Tick(DelataTime);

    const auto AimActor = GetFocusOnActor();
    SetFocus(AimActor);
}

AActor* ABGAIController::GetFocusOnActor() const
{
    if (!GetBlackboardComponent()) return nullptr;
    return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}
