// Bomber Game. All Rights Reserved.

#include "AI/BGAICharacter.h"
#include "AI/BGAIController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BrainComponent.h"

ABGAICharacter::ABGAICharacter()
{
    AutoPossessAI = EAutoPossessAI::Disabled;
    AIControllerClass = ABGAIController::StaticClass();

    if (GetCharacterMovement())
    {
        GetCharacterMovement()->bUseControllerDesiredRotation = true;
    }
}

void ABGAICharacter::OnDeath()
{
    Super::OnDeath();

    const auto BGAIController = Cast<AAIController>(Controller);
    if (BGAIController && BGAIController->BrainComponent)
    {
        BGAIController->BrainComponent->Cleanup();
    }
}
