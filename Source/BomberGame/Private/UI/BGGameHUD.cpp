// Bomber Game. All Rights Reserved.

#include "UI/BGGameHUD.h"
#include "Engine/Canvas.h"
#include "UI/BGBaseWidget.h"
#include "BGGameModeBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogBGGameHUD, All, All)

void ABGGameHUD::DrawHUD()
{
    Super::DrawHUD();
}

void ABGGameHUD::BeginPlay()
{
    Super::BeginPlay();

    //GameWidgets.Add(EBGMatchState::InProgress, CreateWidget<UBGBaseWidget>(GetWorld(), PlayerHUDWidgetClass));

    //for (auto GameWidgetPair : GameWidgets)
    //{
    //    const auto GameWidget = GameWidgetPair.Value;
    //    if (!GameWidget) continue;

    //    GameWidget->AddToViewport();
    //    GameWidget->SetVisibility(ESlateVisibility::Hidden);
    //}


    auto PlayerHUDWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDWidgetClass);
    if(PlayerHUDWidget)
    {
        PlayerHUDWidget->AddToViewport();
    }

    if (GetWorld())
    {
        const auto GameMode = Cast<ABGGameModeBase>(GetWorld()->GetAuthGameMode());
        if (GameMode)
        {
            GameMode->OnMatchStateChanged.AddUObject(this, &ABGGameHUD::OnMatchStateChanged);
        }
    }
}

void ABGGameHUD::OnMatchStateChanged(EBGMatchState State)
{
    // if (CurrentWidget)
    //{
    //     CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
    // }

    // if (GameWidgets.Contains(State))
    //{
    //     CurrentWidget = GameWidgets[State];
    // }

    // if (CurrentWidget)
    //{
    //     CurrentWidget->SetVisibility(ESlateVisibility::Visible);
    //     CurrentWidget->Show();
    // }

    UE_LOG(LogBGGameHUD, Display, TEXT("Match state changed: %s"), *UEnum::GetValueAsString(State));
}
