// Bomber Game. All Rights Reserved.

#include "BGDestructible.h"
#include "Kismet/GameplayStatics.h"
#include "Pickups/BasePickup.h"

ABGDestructible::ABGDestructible()
{
    PrimaryActorTick.bCanEverTick = true;
}

void ABGDestructible::BeginPlay()
{
    Super::BeginPlay();
    OnTakeAnyDamage.AddDynamic(this, &ABGDestructible::OnTakeDamage);
}

void ABGDestructible::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void ABGDestructible::OnTakeDamage(
    AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
    UE_LOG(LogTemp, Warning, TEXT("I, %s, took damage!"), *GetName());
    if (ExploseFX)
    {
        UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExploseFX, GetActorLocation(), FRotator::ZeroRotator);
    }

    if (PickupClasses.Num() == 0)
    {
        UE_LOG(LogTemp, Warning, TEXT("PickupClasses is empty! Add some pickup classes!"));
        return;
    }

    const FTransform SpawnTransform(FRotator::ZeroRotator, GetActorLocation());

    TSubclassOf<ABasePickup> PickupClass;
    int32 RandomIndex = FMath::RandRange(0, PickupClasses.Num() - 1);
    PickupClass = PickupClasses[RandomIndex];

    auto PickupActor = GetWorld()->SpawnActorDeferred<ABasePickup>(PickupClass, SpawnTransform);
    if (PickupActor)
    {

        PickupActor->FinishSpawning(SpawnTransform);

        UE_LOG(LogTemp, Display, TEXT("Set Bomb"));
    }
    Destroy();
}