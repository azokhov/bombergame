// Bomber Game. All Rights Reserved.

#include "BGBomb.h"
#include "Components/BoxComponent.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

ABGBomb::ABGBomb()
{
    PrimaryActorTick.bCanEverTick = true;

    BoxCollisionComponent = CreateDefaultSubobject<UBoxComponent>("BombBoxCollision");
    BoxCollisionComponent->InitBoxExtent(BoxSize);
    SetRootComponent(BoxCollisionComponent);

    // Set collision responses
    BoxCollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
    BoxCollisionComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);
}

void ABGBomb::BeginPlay()
{
    Super::BeginPlay();

    // CollisionComponent->IgnoreActorWhenMoving(GetOwner(), true); ???

    OnTakeAnyDamage.AddDynamic(this, &ABGBomb::OnTakeDamage);
    SetLifeSpan(BombTimer);
}

void ABGBomb::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void ABGBomb::NotifyActorBeginOverlap(AActor* OtherActor)
{
    Super::NotifyActorBeginOverlap(OtherActor);

    if (OtherActor && OtherActor->IsOwnedBy(GetOwner()))
    {
        if (!bPlayerInside)
        {
            // Allow player to pass through while overlapping
            BoxCollisionComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);
            bPlayerInside = true;
        }
    }
    else
    {
        // If another character tries to enter while the player is inside, block them
        if (bPlayerInside)
        {
            BoxCollisionComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
        }
    }
}

void ABGBomb::NotifyActorEndOverlap(AActor* OtherActor)
{
    Super::NotifyActorEndOverlap(OtherActor);

    if (OtherActor && OtherActor->IsOwnedBy(GetOwner()))
    {
        if (bPlayerInside)
        {
            // Block player after overlap ends
            BoxCollisionComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
            bPlayerInside = false;
        }
    }
}

void ABGBomb::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    Super::EndPlay(EndPlayReason);

    if (EndPlayReason == EEndPlayReason::Destroyed)
    {
        Explode();
    }
}

void ABGBomb::MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd)
{
    if (!GetWorld()) return;

    FCollisionQueryParams CollisionParams;
    CollisionParams.AddIgnoredActor(this);

    GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionParams);
}

void ABGBomb::Explode()
{
    FHitResult HitResult;
    FVector TraceStart, TraceEnd;

    TraceStart = GetActorLocation();
    TraceEnd = TraceStart + FVector::ForwardVector * BlastLength;

    FQuat RotationQuat = FQuat(FVector::UpVector, FMath::DegreesToRadians(90.0f));

    for (int32 i = 0; i < 4; ++i)
    {
        MakeHit(HitResult, TraceStart, TraceEnd);

        FVector TraceFXEnd = TraceEnd;
        if (HitResult.bBlockingHit)
        {
            TraceFXEnd = HitResult.Location;
            TraceFXEnd.Z = GetActorLocation().Z;

            MakeDamage(HitResult);
        }
        SpawnTraceFX(TraceStart, TraceFXEnd);

        TraceEnd = TraceStart + RotationQuat.RotateVector(TraceEnd - TraceStart);
    }
}

void ABGBomb::MakeDamage(FHitResult& HitResult)
{
    float DamageAmount = 10.0f;
    const auto DamagedActor = HitResult.GetActor();
    if (!DamagedActor) return;

    DamagedActor->TakeDamage(DamageAmount, FDamageEvent{}, GetInstigatorController(), this);
}

void ABGBomb::SpawnTraceFX(const FVector& TraceFXStart, const FVector& TraceFXEnd)
{
    const auto TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), TraceFX, TraceFXStart);
    if (TraceFXComponent)
    {
        TraceFXComponent->SetVariableVec3(TraceTargetName, TraceFXEnd);
    }
}

void ABGBomb::OnTakeDamage(
    AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
    Destroy();
    UE_LOG(LogTemp, Warning, TEXT("I, %s, took damage!"), *GetName());
}
