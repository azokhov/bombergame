// Bomber Game. All Rights Reserved.

#include "BGGameModeBase.h"
#include "BGCharacter.h"
#include "BGPlayerController.h"
#include "AIController.h"
#include "BGPlayerState.h"
#include "UI/BGGameHUD.h"
#include "EngineUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogBGGameModeBase, All, All)

ABGGameModeBase::ABGGameModeBase()
{
    DefaultPawnClass = ABGCharacter::StaticClass();
    PlayerControllerClass = ABGPlayerController::StaticClass();
    HUDClass = ABGGameHUD::StaticClass();
    PlayerStateClass = ABGPlayerState::StaticClass();
}

void ABGGameModeBase::StartPlay()
{
    Super::StartPlay();

    SpawnBots();
    CreateTeamInfo();

    CurrentRound = 1;
    StartRound();
    SetMathcState(EBGMatchState::InProgress);
}

void ABGGameModeBase::SpawnBots()
{
    if (!GetWorld()) return;

    for (int32 i = 0; i < GameData.PlayersNum - 1; ++i)
    {
        FActorSpawnParameters SpawnInfo;
        SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

        const auto BGAIController = GetWorld()->SpawnActor<AAIController>(AIControllerClass, SpawnInfo);
        RestartPlayer(BGAIController);
    }
}

UClass* ABGGameModeBase::GetDefaultPawnClassForController_Implementation(AController* InController)
{
    if (InController && InController->IsA<AAIController>())
    {
        return AIPawnClass;
    }
    return Super::GetDefaultPawnClassForController_Implementation(InController);
}

void ABGGameModeBase::Killed(AController* KillerController, AController* VictimController)
{
    const auto KillerPlayerState = KillerController ? Cast<ABGPlayerState>(KillerController->PlayerState) : nullptr;
    const auto VictimPlayerState = VictimController ? Cast<ABGPlayerState>(VictimController->PlayerState) : nullptr;

    if (KillerPlayerState)
    {
        KillerPlayerState->AddKill();
    }

    if (VictimPlayerState)
    {
        VictimPlayerState->AddDeath();
    }

    // StartRespawn(VictimController);
}

void ABGGameModeBase::StartRound()
{
    RoundConundDown = GameData.RoundTime;
    GetWorldTimerManager().SetTimer(GameRoundTimerHandle, this, &ABGGameModeBase::GameTimerUpdate, 1.0f, true);
}

void ABGGameModeBase::GameTimerUpdate()
{
    if (--RoundConundDown == 0)
    {
        GetWorldTimerManager().ClearTimer(GameRoundTimerHandle);

        if (CurrentRound + 1 <= GameData.RoundsNum)
        {
            ++CurrentRound;
            ResetPlayers();
            StartRound();
        }
        else
        {
            GameOver();
        }
    }
}

void ABGGameModeBase::ResetPlayers()
{
    if (!GetWorld()) return;

    for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
    {
        ResetOnePlayer(It->Get());
    }
}

void ABGGameModeBase::ResetOnePlayer(AController* Controller)
{
    if (Controller && Controller->GetPawn())
    {
        Controller->GetPawn()->Reset();
    }
    RestartPlayer(Controller);
    SetPlayerColor(Controller);
}

void ABGGameModeBase::CreateTeamInfo()
{
    if (!GetWorld()) return;

    int32 TeamID = 1;
    for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
    {
        const auto Controller = It->Get();
        if (!Controller) continue;

        const auto PlayerState = Cast<ABGPlayerState>(Controller->PlayerState);
        if (!PlayerState) continue;

        PlayerState->SetTeamID(TeamID);
        PlayerState->SetTeamColor(DetermineColorByTeamID(TeamID));
        FString BotName = FString::Printf(TEXT("Bot%d"), TeamID);
        PlayerState->SetPlayerName(Controller->IsPlayerController() ? "Player" : BotName);
        SetPlayerColor(Controller);
        // TeamID = TeamID == 1 ? 2 : 1;
        TeamID = TeamID + 1;
    }
}

FLinearColor ABGGameModeBase::DetermineColorByTeamID(int32 TeamID) const
{
    if (TeamID - 1 < GameData.TeamColors.Num())
    {
        return GameData.TeamColors[TeamID - 1];
    }
    UE_LOG(LogBGGameModeBase, Warning, TEXT("No color for team ID: %i, set to default: %s"), TeamID, *GameData.DefaultTeamColor.ToString());
    return GameData.DefaultTeamColor;
}

void ABGGameModeBase::SetPlayerColor(AController* Controller)
{
    if (!Controller) return;

    const auto Character = Cast<ABGCharacter>(Controller->GetPawn());
    if (!Character) return;

    const auto PlayerState = Cast<ABGPlayerState>(Controller->PlayerState);
    if (!PlayerState) return;

    Character->SetPlayerColor(PlayerState->GetTeamColor());
}

void ABGGameModeBase::LogPlayerInfo()
{
    if (!GetWorld()) return;

    for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
    {
        const auto Controller = It->Get();
        if (!Controller) continue;

        const auto PlayerState = Cast<ABGPlayerState>(Controller->PlayerState);
        if (!PlayerState) continue;

        PlayerState->LogInfo();
    }
}

void ABGGameModeBase::GameOver()
{
    UE_LOG(LogBGGameModeBase, Display, TEXT("==== GAME OVER ===="));
    LogPlayerInfo();

    for (auto Pawn : TActorRange<APawn>(GetWorld()))
    {
        if (Pawn)
        {
            Pawn->TurnOff();
            Pawn->DisableInput(nullptr);
        }
    }
    SetMathcState(EBGMatchState::GameOver);
}

void ABGGameModeBase::SetMathcState(EBGMatchState State)
{
    if (MatchState == State) return;

    MatchState = State;
    OnMatchStateChanged.Broadcast(MatchState);
}
