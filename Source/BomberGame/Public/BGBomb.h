// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BGBomb.generated.h"

class UBoxComponent;
class UNiagaraSystem;

UCLASS()
class BOMBERGAME_API ABGBomb : public AActor
{
    GENERATED_BODY()

public:
    ABGBomb();

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    UBoxComponent* BoxCollisionComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
    FVector BoxSize{45.0f, 45.0f, 95.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    UNiagaraSystem* TraceFX;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    FName TraceTargetName = "TraceTarget";

    float BlastLength = 100.0f;
    float BombTimer = 5.0f;

    virtual void BeginPlay() override;
    virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
    virtual void NotifyActorEndOverlap(AActor* OtherActor) override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

    void MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd);
    void Explode();
    void MakeDamage(FHitResult& HitResult);
    UFUNCTION()
    void OnTakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);
    void SpawnTraceFX(const FVector& TraceFXStart, const FVector& TraceFXEnd);

public:
    virtual void Tick(float DeltaTime) override;
    void SetBlastLength(float NewLength) { BlastLength = NewLength; };
    void SetBombTimer(float NewTimer) { BombTimer = NewTimer; };

private:
    bool bPlayerInside = false;
};
