// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/BGBaseWidget.h"
#include "BGPlayerHUDWidget.generated.h"

UCLASS()
class BOMBERGAME_API UBGPlayerHUDWidget : public UBGBaseWidget
{
    GENERATED_BODY()
protected:
    UPROPERTY(meta = (BindWidgetAnim), Transient)
    UWidgetAnimation* DamageAnimation;

    virtual void NativeOnInitialized() override;
};
