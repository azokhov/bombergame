// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "BGCoreTypes.h"
#include "BGGameHUD.generated.h"

class UBGBaseWidget;

UCLASS()
class BOMBERGAME_API ABGGameHUD : public AHUD
{
    GENERATED_BODY()

public:
    virtual void DrawHUD() override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UUserWidget> PlayerHUDWidgetClass;

    virtual void BeginPlay() override;

private:
    UPROPERTY()
    TMap<EBGMatchState, UBGBaseWidget*> GameWidgets;

    UPROPERTY()
    UBGBaseWidget* CurrentWidget = nullptr;

    void OnMatchStateChanged(EBGMatchState State);
};
