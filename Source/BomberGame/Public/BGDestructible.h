// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BGDestructible.generated.h"

class UParticleSystem;
class ABasePickup;

UCLASS()
class BOMBERGAME_API ABGDestructible : public AActor
{
    GENERATED_BODY()

public:
    ABGDestructible();

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    UParticleSystem* ExploseFX;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Pickup")
    TArray <TSubclassOf<ABasePickup>> PickupClasses;

    UFUNCTION()
    void OnTakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

    virtual void BeginPlay() override;

public:
    virtual void Tick(float DeltaTime) override;
};
