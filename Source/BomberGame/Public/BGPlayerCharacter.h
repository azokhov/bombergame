// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BGCharacter.h"
#include "BGPlayerCharacter.generated.h"

UCLASS()
class BOMBERGAME_API ABGPlayerCharacter : public ABGCharacter
{
    GENERATED_BODY()

public:
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
    void MoveForward(float Amount);
    void MoveRight(float Amount);
};
