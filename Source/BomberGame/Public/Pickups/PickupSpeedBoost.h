// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Pickups/BasePickup.h"
#include "PickupSpeedBoost.generated.h"

UCLASS()
class BOMBERGAME_API APickupSpeedBoost : public ABasePickup
{
    GENERATED_BODY()

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
    float SpeedModifier = 1.2f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
    float Time = 5.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
    bool bIsTemporary = true;

private:
    virtual bool GivePickupTo(APawn* PlayerPawn) override;
};
