// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Pickups/BasePickup.h"
#include "PickupExplosionRadius.generated.h"

UCLASS()
class BOMBERGAME_API APickupExplosionRadius : public ABasePickup
{
    GENERATED_BODY()

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
    int32 BlastLengthModifier = 1;

private:
    virtual bool GivePickupTo(APawn* PlayerPawn) override;
};
