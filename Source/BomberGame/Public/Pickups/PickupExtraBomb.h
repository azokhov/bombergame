// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Pickups/BasePickup.h"
#include "PickupExtraBomb.generated.h"

UCLASS()
class BOMBERGAME_API APickupExtraBomb : public ABasePickup
{
    GENERATED_BODY()

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
    int32 BombAmount = 1;

private:
    virtual bool GivePickupTo(APawn* PlayerPawn) override;
};
