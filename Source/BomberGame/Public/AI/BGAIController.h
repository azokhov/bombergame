// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BGAIController.generated.h"

class UBGAIPerceptionComponent;

UCLASS()
class BOMBERGAME_API ABGAIController : public AAIController
{
    GENERATED_BODY()

public:
    ABGAIController();

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    UBGAIPerceptionComponent* BGAIPerceptionComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    FName FocusOnKeyName = "EnemyActor";

    virtual void OnPossess(APawn* InPawn) override;
    virtual void Tick(float DelataTime) override;

private:
    AActor* GetFocusOnActor() const;
};
