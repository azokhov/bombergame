// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BGCharacter.h"
#include "BGAICharacter.generated.h"

class UBehaviorTree;

UCLASS()
class BOMBERGAME_API ABGAICharacter : public ABGCharacter
{
    GENERATED_BODY()

public:
    ABGAICharacter();

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
    UBehaviorTree* BehaviorTreeAsset;

protected:
    virtual void OnDeath() override;
};
