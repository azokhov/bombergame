// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BGSetBombService.generated.h"

UCLASS()
class BOMBERGAME_API UBGSetBombService : public UBTService
{
    GENERATED_BODY()

public:
    UBGSetBombService();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    FBlackboardKeySelector EnemyActorKey;

    virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
