// Bomber Game. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BGCharacter.generated.h"

class ABGBomb;

UCLASS()
class BOMBERGAME_API ABGCharacter : public ACharacter
{
    GENERATED_BODY()

public:
    ABGCharacter();

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Material")
    FName MaterialColorName = "BodyColor";
    UPROPERTY(EditDefaultsOnly, Category = "Damage")
    float LifeSpanOnDeath = 5.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
    float GridSize = 100.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
    float HightOfBombSpawn = 120.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bomb")
    TSubclassOf<ABGBomb> BombActorClass;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bomb")
    int32 MaxBombCount = 2;
    int32 CurrentBombCount = 0;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bomb")
    int32 CurrentBlastLengthModifier = 2;
    float DefaultSpeed = 600.0f;
    FTimerHandle SpeedTimerHandle;
    float SpeedModifier = 1.0f;

    bool bCanSetBomb = true;
    bool bIsDead = false;

    UFUNCTION()
    void OnTakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);
    UFUNCTION()
    void IncreaseSpawnedBombCount();
    UFUNCTION()
    void DecreaseSpawnedBombCount(AActor* DestroyedActor);
    void CheckSpawnedBombCount();

    virtual void BeginPlay() override;
    virtual void OnDeath();

public:
    virtual void Tick(float DeltaTime) override;

    void SetBomb();

    bool TryToAddBomb(int32 BombAmount);
    bool TryToAddBlastLengthModifier(int32 AddModifier);
    bool TryToSetNewSpeed(float NewSpeedModifier, bool IsTemporary, float Time);
    void SetDefaultSpeed();
    bool IsAlive() { return !bIsDead; };

    void SetPlayerColor(const FLinearColor& Color);

private:
    FVector GetGridLocation(const FVector& NonGridLocation, float SizeOfGrid);

    void Killed(AController* KilledController);
};
